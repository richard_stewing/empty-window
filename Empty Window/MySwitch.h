//
//  MySwitch.h
//  Empty Window
//
//  Created by Richard Stewing on 26.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySwitch : UISwitch

-(IBAction)MySwitchEvent:(id)sender;

@end
