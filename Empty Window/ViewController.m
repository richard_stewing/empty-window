//
//  ViewController.m
//  Empty Window
//
//  Created by Richard Stewing on 23.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import "ViewController.h"
#import "MyClass.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) buttonPressed :(id) sender{
    MyClass* mc = [MyClass new];
    [mc log];
    UIAlertView* av = [[UIAlertView alloc] initWithTitle:@"Howdy" message:@"You tapped me" delegate:self cancelButtonTitle:@"cool" otherButtonTitles:@"test", nil];
    [av show];
    
}
-(void) alertView: (UIAlertView* ) av
didDismissWithButtonIndex: (NSInteger* )ix{
    if(ix == 1){
        NSLog(@"alert");
        
        
    }
    
}


@end
