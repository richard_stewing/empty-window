//
//  MySwitch.m
//  Empty Window
//
//  Created by Richard Stewing on 26.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import "MySwitch.h"

@implementation MySwitch{
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (IBAction)MySwitchEvent:(id)sender {
    NSLog(@"From MySwitch.m");
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
