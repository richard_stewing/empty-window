//
//  MyData.h
//  Empty Window
//
//  Created by Richard Stewing on 27.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyData : NSObject
-(NSInteger*)test;
-(void)setTest:withValue;

@end
