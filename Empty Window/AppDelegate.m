//
//  AppDelegate.m
//  Empty Window
//
//  Created by Richard Stewing on 23.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import "AppDelegate.h" 
#import "MyClass.h"
#import "MySwitch.h"
#import "MyView.h"
#import "MyData.h"



@implementation AppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.mv = [MyView new];
    [[NSBundle mainBundle] loadNibNamed:@"MyNib" owner:self.mv options:nil];
    MyLabel* lab = self.mv.theLabel;
    MyClass* but = self.mv.button;
    MySwitch* Switch = self.mv.mySwitch;
    Switch.center = CGPointMake(150, 300);
    Switch.frame = CGRectIntegral(Switch.frame);
    but.center = CGPointMake(150, 200);
    but.frame = CGRectIntegral(but.frame);
    lab.center = CGPointMake(150, 100);
    lab.frame = CGRectIntegral(lab.frame);
    [self.window.rootViewController.view addSubview:lab];
    [self.window.rootViewController.view addSubview:but];
    [self.window.rootViewController.view addSubview:Switch];
    [but addTarget:but action:@selector(MyButtonEvent:) forControlEvents: UIControlEventTouchUpInside];
    [Switch addTarget:Switch action:@selector(MySwitchEvent:) forControlEvents: UIControlEventValueChanged];
    NSLog(@"Application launched\n");
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"will be in background");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"in Background");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"will be back in forground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"active");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"will terminate, prpably never called");
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (IBAction)MyButtonEvent:(id)sender {
    [[MyClass new] MyButtonEvent: nil];
}

@end
