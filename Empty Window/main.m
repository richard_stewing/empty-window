//
//  main.m
//  Empty Window
//
//  Created by Richard Stewing on 23.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "MyClass.h"

int main(int argc, char *argv[])
{
    printf("test\n");
    @autoreleasepool {
        return UIApplicationMain(argc, argv,   nil,  NSStringFromClass([AppDelegate class]));
    }
    
}
