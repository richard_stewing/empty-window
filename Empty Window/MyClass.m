//
//  MyClass.m
//  Empty Window
//
//  Created by Richard Stewing on 24.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import "MyClass.h"
#import "MyData.h"
int count = 0;
@implementation MyClass{
   
}

- (IBAction)MyButtonEvent:(id)sender {
    NSLog(@"From MyCLass.m");
    count++;
    [self log];
}

-(void) log{
    printf("%d\n", count);
    //NSLog(@"Richard");
}



@end
