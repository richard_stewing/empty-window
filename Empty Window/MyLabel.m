//
//  MyLabel.m
//  Empty Window
//
//  Created by Richard Stewing on 24.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import "MyLabel.h"

@implementation MyLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void) awakeFromNib{
    [super awakeFromNib];
    self.text = @"i iniy myself";
    [self sizeToFit];
}


@end
