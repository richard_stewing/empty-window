//
//  ViewController.h
//  Empty Window
//
//  Created by Richard Stewing on 23.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
-(void) alertView;
-(IBAction)buttonPressed:(id)sender;

@end
