//
//  AppDelegate.h
//  Empty Window
//
//  Created by Richard Stewing on 23.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyClass.h"
#import "MyView.h"
#import "MySwitch.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MyView* mv;

@end