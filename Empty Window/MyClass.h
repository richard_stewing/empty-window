//
//  MyClass.h
//  Empty Window
//
//  Created by Richard Stewing on 24.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MyClass : UIButton
-(void) log;
-(IBAction)MyButtonEvent:(id)sender;
@property (nonatomic) int count;

@end
