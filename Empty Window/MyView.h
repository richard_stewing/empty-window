//
//  MyView.h
//  Empty Window
//
//  Created by Richard Stewing on 26.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MySwitch.h"
#import "MyLabel.h"
#import "MyClass.h"


@interface MyView : UIView
@property (strong, nonatomic) IBOutlet MySwitch* mySwitch;
@property (strong, nonatomic) IBOutlet MyClass* button;
@property (strong, nonatomic) IBOutlet MyLabel* theLabel;

@end
